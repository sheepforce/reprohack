let
  nixpkgs = import ../nixpkgs.nix {};
  rPkgs = import ./pkgs.nix { inherit (nixpkgs) rPackages; };

in with nixpkgs; mkShell {
  buildInputs = [
    (rstudioWrapper.override { packages = rPkgs; })
    (rWrapper.override { packages = rPkgs; })
    pandoc
  ];
}

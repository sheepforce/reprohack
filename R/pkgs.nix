{ rPackages }:

with rPackages; [
  DT
  ggplot2
  ggpubr
  readxl
  stringr
  rworldmap
  RColorBrewer
  plyr
  tidyr
  rmdformats
  knitr
]

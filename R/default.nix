{ stdenvNoCC, lib, fetchurl, unzip, rPackages, rWrapper, pandoc }:

let
  rPkgs = import ./pkgs.nix { inherit rPackages; };
  rWithPkgs = rWrapper.override { packages = rPkgs; };

in stdenvNoCC.mkDerivation rec {
  pname = "ReproHack-Original";
  version = "1.0";

  src = fetchurl {
    url = "https://s3-eu-west-1.amazonaws.com/pfigshare-u-files/22720802/SupplementaryMaterial.zip";
    sha256 = "00lsp163q44dn8adlra8vaf4cgcyiifv2nh0qabypsfcgzj0c2sd";
  };

  nativeBuildInputs = [
    pandoc
    rWithPkgs
    unzip
  ];

  phases = [
    "unpackPhase"
    "buildPhase"
    "installPhase"
  ];

  installTargets = [
    "Survey_Trend_SDI.html"
    "Survey_Trend_SDI_files"
  ];

  unpackPhase = ''
    unzip $src
  '';

  buildPhase = ''
    Rscript -e "library(knitr); rmarkdown::render('Survey_Trend_SDI.Rmd')"
  '';

  installPhase = ''
    mkdir -p $out
    for i in ${toString installTargets}; do
      cp -r $i $out/.
    done
  '';
}

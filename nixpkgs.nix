{
  # The Haskell.nix repository with the Haskell infrastructure. Defaults to the current master.
  haskellNix ? import (builtins.fetchGit {
    name = "haskell.nix_10.05.2021";
    url = "https://github.com/input-output-hk/haskell.nix";
    rev = "ecfd77b39b00a863ef82812e6824a32072dd945e";
    ref = "master";
  }) {}

  # A Nixpkgs channel. Defaults to a rebuild version channel of nixos-20.09 from Haskell.nix,
  # but also <nixos>, ... are possible.
, nixpkgsSrc ? haskellNix.sources.nixpkgs-unstable
#, nixpkgsSrc ? (builtins.fetchGit {
#    name = "nixpkgs-unstable_10.05.2021";
#    url = "https://github.com/NixOS/nixpkgs";
#    rev = "8cba5c986b1b6a1efbc4b62eca059e43aed895e4";
#    ref = "nixpkgs-unstable";
#  })

  # QChem overlay for additional libraries and chemistry software.
, qchem ? import (builtins.fetchGit {
    name = "nixos-qchem_10.05.2021";
    url = "https://github.com/markuskowa/NixOS-QChem";
    rev = "c56aa353b447b0aed2e840e4bc95b105cd0a1eb3";
    ref = "master";
  })
}:
let
  overlays = haskellNix.nixpkgsArgs.overlays ++ [ qchem ];
  pkgs = import nixpkgsSrc (haskellNix.nixpkgsArgs // { inherit overlays; });

in pkgs

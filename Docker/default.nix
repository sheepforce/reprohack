{ pkgs ? import ../nixpkgs.nix {} }:

let
  hsPkg = import ../Haskell/default.nix { inherit pkgs; };

in with pkgs; dockerTools.buildImage {
  name = "ReproHack";
  config = {
    Cmd  = [ "${hsPkg.ReproHack.components.exes.reprohack}/bin/reprohack" ];
  };
}

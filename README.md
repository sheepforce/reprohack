# ReproHack

## The Repo
You will need a fork of the repo and a cross-fork merge request to merge anything into this repo.

## Nix Infrastructure
The file `nixpkgs.nix` defines the nix package set used throughout the repository.
It pins a recent version of `nixpkgs-unstable` and therefore is fully reproducible.
To search for available packages in `nixpkgs-unstable` refer to the [NixOS search](https://search.nixos.org/packages).

In the subdirectories `Haskell`, `Docker`, `Python` and `R` are specific nix expressions for development with those frameworks/languages.
For R and Python the `pkgs.nix` defines the packages available to a development shell.
Just add the ones you require at the corresponding `pkgs.nix`.
Haskell follows the [Haskell.nix](https://github.com/input-output-hk/haskell.nix) build style and the build system and available packages are managed by the `package.yaml` and `ReproHack.cabal` file.

Within the `Haskell`, `Python` and `R` directories you can simply obtain a development shell by executing `nix-shell`.
There won't be any `pip` available in the Python dev shell and it also won't be necessary.
Therefore, there is also no need to use a virtual environment or anything, this is all done by Nix.

Within the `Docker` and `Haskell` directories is also the `default.nix` file, which describes how to build a Nix derivation from this directory.
Those can be realised by `nix-build` within those directories, e.g. `nix-build` within the `Docker` directory will create a Docker container.
To build the executable of the Haskell project execute `nix-build -A ReproHack.components.exes` within the `Haskell` directory.

## Building
### Original Data
To build the original data exactly execute
```bash
nix-build -A rOriginal
```
in the top level directory.
You will get a symlink `result` with output data.

### Python Notebook
To get a working jupyter notebook, within the `Python` directory execute:
```bash
nix-shell --command "jupyter lab"
```
and your browser will open a jupyter lab interface.
Open the `analysis.ipynb`, select the `ReproHackPython` kernel and have fun with the notebook.

{ pkgs ? import ../nixpkgs.nix {} }:

with pkgs; haskell-nix.project {
  src = builtins.path {
    name = "ReproHack";
    path = ./.;
  };

  compiler-nix-name = "ghc8104";
}

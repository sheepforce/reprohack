{ python3 }:

python3.withPackages (p: with p; [
  numpy
  pandas
  geopandas
])

{ nixpkgs ? import ../nixpkgs.nix {}
, pythonPkgs ? import ./pkgs.nix { inherit (nixpkgs) python3; }

  # Juypter Notebooks
, jupyter ? import (builtins.fetchGit {
    name = "jupyterWith_11.05.2021";
    url = "https://github.com/tweag/jupyterWith";
    rev = "b410ed1f6f78f1890a66eada5063731ae7eef8e5";
    ref = "master";
  }) { }
}:

let
  nixpkgs = import ../nixpkgs.nix {};
  pythonPkgs = import ./pkgs.nix { inherit (nixpkgs) python3; };

  iPython = jupyter.kernels.iPythonWith {
    name = "ReproHackPython";
    packages = p: with p; [
      pandas
      matplotlib
      geopandas
    ];
  };

  jupyterEnvironment = jupyter.jupyterlabWith {
    kernels = [ iPython ];
  };


in jupyterEnvironment.env
